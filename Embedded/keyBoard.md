
#Tools

[Keyboard Layout Editor](http://www.keyboard-layout-editor.com/#/):键盘布局编辑

[Plate & Case Builder](http://builder.swillkb.com/):将键盘布局制作为板框文件

# QMK

[客制化键盘QMK固件入门-修改keyboard和keymap](https://baijiahao.baidu.com/s?id=1665730272327907789&wfr=spider&for=pc)

[新手小白修复路透社机械键盘——超详细QMK刷机教程](https://post.smzdm.com/p/aekz8pdm/)

[怎么对键盘进行测试？](https://docs.qmk.fm/#/zh-cn/faq_misc)

[How a Keyboard Matrix Works](https://docs.qmk.fm/#/how_a_matrix_works)

[tmk keyboard](https://github.com/tmk/tmk_keyboard/tree/master/tmk_core)

[qmk firmware](https://github.com/qmk/qmk_firmware)

[怎么对键盘进行测试？](https://docs.qmk.fm/#/zh-cn/faq_misc)

[键盘测试](https://config.qmk.fm/#/test/)

[Understanding QMK’s Code](https://docs.qmk.fm/#/zh-cn/understanding_qmk)

[Keycodes Overview](https://docs.qmk.fm/#/keycodes)

[qmk distro msys](https://github.com/qmk/qmk_distro_msys)

[Plate & Case Builder](https://config.qmk.fm/#/10bleoledhub/LAYOUT)

# 键盘基础

[Windows和Mac键盘有什么区别 Mac上如何使用Windows键盘](https://www.jb51.net/softjc/705487.html)

[Mac和Windows键盘对应关系](https://www.csdn.net/tags/NtDaUg0sMzU2OC1ibG9n.html)

[mac键位的修改，使其和win尽量相同](https://www.kancloud.cn/q952008898/hei_ping_guo/1053362)

[反射式红外光电检测管 ： ITR9909](https://blog.csdn.net/zhuoqingjoking97298/article/details/120209016)


# 资料收录收录

[自制的机械键盘pcb，104键+16键，灯效正在开发](https://v.youku.com/v_show/id_XMzcxMTYxMDkyMA==.html?spm=a2hbt.13141534.0.13141534)

[键盘键值表KeyCode键盘键值对照表](https://www.cnblogs.com/jcserver/articles/2571112.html)

[「海盗船k68RGB机械键盘」海盗船k68红轴机械键盘介绍及wallpaper音乐律动效果展示教学](https://haokan.baidu.com/v?pd=wisenatural&vid=2431452178251955646)

[STM32CubeMX学习笔记（24）——通用定时器接口使用（电容按键检测）](https://blog.csdn.net/qq_36347513/article/details/115303731)

[码农的自我修养 - USB键盘和鼠标的数据包格式](https://blog.csdn.net/guoqx/article/details/122020615)

[别让键盘限制你的游戏发挥：关于游戏键盘的低延迟技术和讲解](https://www.bilibili.com/read/cv7465854/)

[【低功耗蓝牙】⑤ HID协议](https://www.bilibili.com/read/cv15067064)

[BLE HID简介](https://blog.csdn.net/lep150510/article/details/119571510)


# 开源工程

[基于ATMEGA32U4主控的96配列键盘](https://oshwhub.com/SongJiHyo/jian-pan96)

[客制化键盘-gh60(改版无灯)](https://oshwhub.com/KINOo/ke-zhi-hua-jian-pan_gh60-gai-ban-wu-deng)

[#第七届立创电赛#最丐68+4机械键盘PRO](https://oshwhub.com/yangzen/zui-gai68-)



# 机械键盘

[全键无冲功能测试](http://www.smartyao.com/page520.html)

[机械键盘WIN键锁定什么意思 我的是凯酷 红轴](https://zhidao.baidu.com/question/474291499.html)

[酷冷至尊袖刃CK721机械键盘评测：三模自由切换！68键五脏俱全](https://baijiahao.baidu.com/s?id=1724201145891608972&wfr=spider&for=pc)

[键盘宏如何设置？](https://jingyan.baidu.com/article/f96699bb846ce3c84e3c1bfc.html)

[机械键盘特殊灯光效果怎么调？](https://www.zhihu.com/question/268467888?sort=created)

[8120年了，USB键盘还只能6键无冲？](https://www.bilibili.com/read/cv1480188/)

[USB键盘，每次发送给主机的是8位的信号，最多6键同时按下，那么机械键盘的全键无冲是如何做到的？](https://www.zhihu.com/question/59037453/answer/1690437668)

[USB原理：从零基础入门到放弃](https://blog.csdn.net/jimaofu0494/article/details/109233087)

[什么是光轴(键盘轴体)?](https://www.zhihu.com/question/339620173)

[宏录制，热插拔，RGB背光，这竟是一把入门机械键盘](https://baijiahao.baidu.com/s?id=1692583551917929843&wfr=spider&for=pc)

# 卫星轴

[键盘所谓的卫星轴是指什么呀？](https://www.zhihu.com/question/29130257?ivk_sa=1024320u)

[2022年最佳键盘推荐，专为学生党/程序员/码字/游戏人群打造的最强键盘攻略，Keychron、腹灵、杜伽、罗技、iQunix、FILCO......（3月持续更新）](https://zhuanlan.zhihu.com/p/430942833)

[傻狗键谈之浅谈卫星轴](https://www.zfrontier.com/app/flow/eVz5jvVvmBZR)

[填一下之前开的吐槽国产静电容的坑](https://www.bilibili.com/read/cv7214709)


# USB HID多媒体键盘

[USB HID多媒体键盘描述符](https://www.diluowu.com/archives/8.html)

[沁恒CH552E USB-HID自定义键盘带多媒体功能](https://dl.21ic.com/download/ch552eusb-379263.html)

[请教如何在USB HID（键盘）设备中增加静音或者播放等功能？](https://www.amobbs.com/thread-5664584-1-1.html)

[在万利的STM32学习板上实现USB多媒体键盘](http://home.eeworld.com.cn/my/space-uid-77542-blogid-12907.html)

[客制pc小键盘，CH552单片机HID键盘，电脑多媒体](https://oshwhub.com/TheLight/zi-ding-yi-jian-pan)

[HID键盘值（包含多媒体键）](https://feelsight.cn/post/117.html)

[STM32F103C8T6-Keypad-59](https://github.com/srduino/STM32F103C8T6-Keypad-59)

[枚举 USB 复合设备](https://docs.microsoft.com/zh-cn/windows-hardware/drivers/usbcon/enumeration-of-the-composite-parent-device)

# HID复合设备

[USB-HID设备中的复合设备](https://blog.csdn.net/gd6321374/article/details/79919917)

[USB复合设备（mass storage&hid）](https://blog.csdn.net/plauajoke/article/details/8537740)

[USB Compound Device，USB复合设备 ； USB Composite Device，USB组合设备](http://www.360doc.com/content/19/1029/15/32066980_869774990.shtml)

# github

[keyboard](https://github.com/SEASKY-Master/keyboard)

[STM32-Mechanical-Keyboard](https://github.com/alxhoff/STM32-Mechanical-Keyboard)
 
[keyboard-stm32](https://github.com/vitalyvb/keyboard-stm32)

[USB Device Tree Viewer V3.7.7](https://www.uwe-sieber.de/usbtreeview_e.html)

[Windows-driver-samples](https://github.com/Microsoft/Windows-driver-samples/tree/master/usb/usbview)

[STM32硬核DIY机械键盘|蓝牙USB双模|灯控](https://blog.csdn.net/yougeng123/article/details/103803593)

[半自制机械键盘](https://gitee.com/feoar/project_1?_from=gitee_search#https://gitee.com/link?target=https%3A%2F%2Fkbfirmware.com%2F)

[极度硬核DIY蓝牙机械键盘 USB蓝牙双模|从零开始|制作全过程](https://www.bilibili.com/video/av81005730#reply2225849370)

[DIY机械键盘-PCB-程序](https://www.bilibili.com/video/BV1ri4y1t7xG)

[OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB)

[OpenRGB Plugins](https://openrgb.org/plugins.html)

# LED
[WS2812B STM32F4](https://github.com/hubmartin/WS2812B_STM32F4)

[ws2812 controller.ino](https://github.com/segfault16/modular-led-controller-workstation/blob/master/arduino/ws2812_controller/ws2812_controller.ino)

[Arduino-FastLED-Music-Visualizer](https://github.com/justcallmekoko/Arduino-FastLED-Music-Visualizer)

[polychromatic](https://gitee.com/mirrors/polychromatic?_from=gitee_search)

[polychromatic](https://github.com/polychromatic/polychromatic)


# Tools

[Keyboard Layout Editor-生成键盘布局](http://www.keyboard-layout-editor.com/#/)

[Plate & Case Builder-生成键盘定位](http://builder.swillkb.com/)

# USB

[怎样获取USB的设备描述符数据？](https://www.zhihu.com/question/357732458)

[USB Device Tree Viewer(USB接口查看器) v3.7.3 免费绿色版 64位](https://www.jb51.net/softs/356526.html)

[usblyzer破解版v2.2](http://www.xue51.com/soft/6739.html)

[USB 协议分析之 HID 设备](https://blog.csdn.net/zhoutaopower/article/details/82469665)

[hid键盘报告格式](https://blog.csdn.net/a65135793/article/details/80287250)

[USB-HID鼠标、键盘通讯格式(转) 与本人实际测试结果](https://www.cnblogs.com/vonly/p/7403823.html)

[HID Descriptor Tool](https://www.usb.org/document-library/hid-descriptor-tool)

[USB的设备描述符详解-具体含义](http://www.mcublog.cn/software/2020_03/usb-devicedesc/)

[USB HID报告描述符生成工具-使用说明](http://www.mcublog.cn/software/2020_03/bgmsf-scgj/)

[USB的配置描述符详解](http://www.mcublog.cn/software/2020_03/usb-configuration/)

[USB的接口描述符详解](http://www.mcublog.cn/software/2020_03/usb-interface/)

[USB的端点描述符详解](http://www.mcublog.cn/software/2020_03/usb-endpoint/)

[USB的HID描述符详解](http://www.mcublog.cn/software/2020_03/usb-hiddescriptor/)

[usb hid 报告描述符（转）](https://www.cnblogs.com/kernel-style/articles/3208922.html)

[USB 协议分析之 HID 设备](https://www.csdn.net/tags/MtTaEgysMDY5ODM1LWJsb2cO0O0O.html)

[HID用图表大纲](https://blog.csdn.net/qq_40088639/article/details/120850181)


# 硬件

[MOS管：N-MOS和P-MOS区别](https://baijiahao.baidu.com/s?id=1724352826134720032&wfr=spider&for=pc)

[MOS管基本认识（快速入门）](https://blog.csdn.net/baidu_19356259/article/details/80921524)

[红外发射接收对管电路](https://www.elecfans.com/d/1063734.html)

[Micro USB 引脚定义及OTG (USB-HOST) 接线](https://blog.csdn.net/smxdhb/article/details/23751625)

[BK2535-2.4G 无线芯片](http://www.bekencorp.com/index/goods/detail/cid/8.html)

[PAW3205DB-鼠标传感器](https://pdf1.alldatasheetcn.com/datasheet-pdf/view/558655/PIXART/PAW3205DB-TJ3T.html)

# 灯效

[『退烧选择』戳中我心的高颜值紧凑配列「御斧R100」金粉轴V2量产键盘～](https://www.bilibili.com/video/BV1P44y1P7zG?spm_id_from=333.337.search-card.all.click)

[96配列机械键盘音乐律动](https://www.bilibili.com/video/BV145411J7jX?spm_id_from=333.337.search-card.all.click)

[【测试】罗技G910音乐律动效果](https://www.bilibili.com/video/BV1bW411J74o/?spm_id_from=333.788.recommend_more_video.1)

[【开源】百元造音乐频谱仪，让音乐跳动起来，全开源](https://www.bilibili.com/video/BV1HS4y1f7FF?spm_id_from=333.999.0.0)

[键盘灯光秀](https://www.douyin.com/video/7175768106323823929)

[C语言代码雨效果](http://www.qb5200.com/article/488958.html)


