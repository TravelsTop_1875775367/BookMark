
# GitHub

[arduinoFFT](https://github.com/kosme/arduinoFFT)

#FFT Library

[Arduino FFT Library](http://wiki.openmusiclabs.com/wiki/FFTFunctions)



#音乐频谱

[STM32F103+全彩LED显示屏+万年历闹钟+FFT音频频谱制作](https://blog.csdn.net/u011504434/article/details/119296746)

[9简单咪头放大电路图大全（五款咪头放大电路）](https://www.elecfans.com/dianlutu/187/20180428669767.html)

[MEMS硅麦和ECM驻极体麦](https://blog.csdn.net/weixin_41624658/article/details/107562328)

[驻极体话筒应用电路](https://www.renrendoc.com/paper/170704304.html)

[驻极体式MIC电路设计](https://blog.csdn.net/ASKLW/article/details/121569685)

[敏芯微硅麦克风MSM42A3722H1-V1.4-ENG参数规格书](https://www.docin.com/p-2100066367.html)

[4脚耳机插座使用说明及如何接线](https://zhidao.baidu.com/question/878891251882872652.html)


#视频

[stm32单片机，FFT音乐频谱，音频频谱，万年历闹钟，LED显示屏](https://www.bilibili.com/video/BV1dw41197LB?spm_id_from=333.337.search-card.all.click)

[STM32驱动LED点阵屏显示音乐频谱+电子时钟](https://www.bilibili.com/video/BV1u3411579c?spm_id_from=333.337.search-card.all.click)

[【开源】百元造音乐频谱仪，让音乐跳动起来，全开源](https://www.bilibili.com/video/BV1HS4y1f7FF?spm_id_from=333.999.0.0)

[【DIY】手把手教你制作桌面像素音频灯](https://www.bilibili.com/video/BV1yU4y157zN/?spm_id_from=333.788.recommend_more_video.-1)

[【AWTRIX PRO】一起动手做一个高颜值的像素灯](https://www.bilibili.com/video/BV1yU4y157zN/?spm_id_from=333.788.recommend_more_video.-1)
