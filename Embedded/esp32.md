
# ESP32

[ESP32-PICO-D4 从入门到进阶——1、软件安装](https://zhuanlan.zhihu.com/p/463743298)

[ESP32­WROOM­32E ESP32­WROOM­32UE 技术规格书](https://www.espressif.com.cn/sites/default/files/documentation/esp32-wroom-32e_esp32-wroom-32ue_datasheet_cn.pdf)

# espressif

[I2S](https://docs.espressif.com/projects/esp-idf/zh_CN/v3.2.5/api-reference/peripherals/i2s.html) : i2s_pop_sample

[与 ESP32 创建串口连接](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/get-started/establish-serial-connection.html)

[Windows 平台工具链的标准设置](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/get-started/windows-setup.html#get-started-windows-first-steps)

[芯片系列对比](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/hw-reference/chip-series-comparison.html)

[ESP32­PICO­D4技术规格书](https://www.espressif.com.cn/sites/default/files/documentation/esp32-pico-d4_datasheet_cn.pdf)

[ESP32 系列芯片技术规格书](https://www.espressif.com.cn/sites/default/files/documentation/esp32_datasheet_cn.pdf)

[ESP32技术参考手册](https://www.espressif.com.cn/sites/default/files/documentation/esp32_technical_reference_manual_cn.pdf)

[VS Code IDE 快速入门](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32c3/get-started/vscode-setup.html)

[ESP32硬件设计指南](https://www.espressif.com.cn/sites/default/files/documentation/esp32_hardware_design_guidelines_cn.pdf)

# 验证板

[ESP32-PICO-KIT V4/V4.1 入门指南](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/hw-reference/esp32/get-started-pico-kit.html)

[ESP32-PICO-KIT V4/V4.1 入门指南](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/hw-reference/esp32/get-started-pico-kit.html#get-started-pico-kit-v4-board-front)

[ESP32-C3-DevKitM-1](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32c3/hw-reference/esp32c3/user-guide-devkitm-1.html)


# GitHub

[esp-idf/components/driver/i2s.c](https://github.com/espressif/esp-idf/blob/v3.2.5/components/driver/i2s.c)

[esp-idf/examples/peripherals/i2s/main/i2s_example_main.c](https://github.com/espressif/esp-idf/blob/v3.2.5/examples/peripherals/i2s/main/i2s_example_main.c)


# 笔记

[ARDUINO ESP32驱动SPI TFT4.0寸屏幕ILI9486+XPT2046使用TFT_eSPI库](https://blog.openpilot.cc/archives/2965)

[ESP32使用Arduino框架驱动树莓派3.5寸LCD屏幕](https://www.guyuehome.com/37119)

[ESP32 + Arduino使用TFT_eSPI库（LCD驱动芯片ST7789）](http://www.manongjc.com/detail/28-hnofmeigfqcizec.html)

[ESP32使用外设RMT控制WS2812灯条](https://blog.csdn.net/tian_milk/article/details/123585610)

[ESP32使用外设RMT控制WS2812灯条](https://blog.csdn.net/tian_milk/article/details/123585610)



