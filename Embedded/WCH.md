# 键盘/HID

[三模键盘开发文档](https://doc-3modekbd.readthedocs.io/zh_CN/latest/firmware/function.html#id4)

[USB鼠标HID报告描述符数据格式分析](https://www.usbzh.com/article/detail-327.html)

[HID类的JoyStick描述符](https://www.cnblogs.com/JayWellsBlog/p/15952721.html)

# BLE/2.4G

[BLE功耗蓝牙之GAP、GATT](https://blog.csdn.net/qq_21842557/article/details/50771077)

[BLE体系结构浅析-基于沁恒CH58X](https://zhuanlan.zhihu.com/p/454057990)

[BLE和2.4G同时运行](https://www.cnblogs.com/debugdabiaoge/p/16149660.html)

[蓝牙 BLE连接参数 连接间隔讲解 connection interval Slave latency timeout CH573 CH582 CH579 peripheral通用外设例子讲解四](https://www.cnblogs.com/debugdabiaoge/p/16055963.html)

[一图读懂 2.4G信道为什么选1611或15913？](https://smb.tp-link.com.cn/service/detail_article_4669.html)

[2.4g无线跳频（一）](https://blog.csdn.net/wangzibigan/article/details/77479044?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522166951649216800192297465%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=166951649216800192297465&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~sobaiduend~default-2-77479044-null-null.nonecase&utm_term=2.4G%E8%B7%B3%E9%A2%91&spm=1018.2226.3001.4450)

[2.4g无线跳频（二）](https://blog.csdn.net/libeiqi1201/article/details/102896896?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522166951649316800192228599%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=166951649316800192228599&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-4-102896896-null-null.nonecase&utm_term=2.4G%E8%B7%B3%E9%A2%91&spm=1018.2226.3001.4450)

[2.4g无线跳频（三）](https://blog.csdn.net/libeiqi1201/article/details/102897037)

[经典蓝牙Inquiry过程的跳频](https://blog.csdn.net/Wendell_Gong/article/details/126959498)

[【低功耗蓝牙】⑤ HID协议](https://www.bilibili.com/read/cv15067064)

[Bushound往设备发送数据](https://www.cnblogs.com/azou/p/17129454.html)


# CH573

[CH573 CH582 用户程序跳转进BOOT](https://www.cnblogs.com/debugdabiaoge/p/15880780.html)

[SCHPCB.ZIP-PCB封装库](https://www.wch.cn/downloads/SCHPCB_ZIP.html)

[CH582M 原理图设计-无线PCB布板规范](https://www.cnblogs.com/debugdabiaoge/p/15748599.html)

[CH579 CH573 CH582 芯片使用知识分享目录](https://www.cnblogs.com/debugdabiaoge/p/15772780.html)

[CH573 CH582 CH579 RISC-V仿真开启及使用](https://www.cnblogs.com/debugdabiaoge/p/15744900.html)


# TMOS

[TMOS](https://www.cnblogs.com/wahahahehehe/p/14752637.html)

[CH579 CH57x 的TMOS系统使用](https://www.cnblogs.com/iot-fan/p/13460082.html)


# Tools

[COMTransmit.ZIP-串口助手](https://www.wch.cn/downloads/COMTransmit_ZIP.html)

