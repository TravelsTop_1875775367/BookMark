
# Microsoft

[针对 USB 设备的 Microsoft OS 描述符](https://docs.microsoft.com/zh-cn/windows-hardware/drivers/usbcon/microsoft-defined-usb-descriptors)

[Microsoft OS 1.0 描述符规范](https://docs.microsoft.com/zh-cn/windows-hardware/drivers/usbcon/microsoft-os-1-0-descriptors-specification)

[Windows 中的 USB ContainerID](https://docs.microsoft.com/zh-cn/windows-hardware/drivers/usbcon/usb-containerids-in-windows)


# WinUSB 

[使枚举成功的USB设备成为WINUSB设备](https://www.it610.com/article/1275294157664960512.htm)

[MM32 USB功能学习笔记 —— WinUSB设备](https://aijishu.com/a/1060000000121851?ivk_sa=1024320u)

[STM32 WinUSB(WCID)免驱高速通信 20M/s（附详细测试）](https://blog.csdn.net/bingwueyi/article/details/121622001)

[基于WinUSB实现的嵌入式USB免驱设备通信方式](http://bbs.ntpcb.com/read-htm-tid-127398.html)

[WinUSB - 微软为所有 USB 设备提供的常规驱动程序](https://blog.csdn.net/whw8007/article/details/9569989)

[用stm32实现winusb组合设备时遇到的一些坑](https://blog.csdn.net/qq_42907191/article/details/119608997)

[MM32 USB功能学习笔记 —— WinUSB设备](https://aijishu.com/a/1060000000121851?ivk_sa=1024320u)

[让嵌入式设备枚举成WinUSB设备](http://bbs.eeworld.com.cn/thread-1067292-1-1.html)

# GitHub

[TeenyUSB](https://github.com/xtoolbox/TeenyUSB/wiki/WCID-Device)

[TeenyDT - Online USB Descriptor Generator](https://xtoolbox.gitee.io/teenydt/)

[libwdi](https://github.com/pbatard/libwdi/wiki/WCID-Devices)

