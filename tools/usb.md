
# USB

[STM32 软件复位并模拟USB拔插](https://www.cnblogs.com/xingboy/p/9872381.html)

[从零开始学USB（十八、USB的class）](https://www.csdn.net/tags/NtzaAg2sNzMzNjgtYmxvZwO0O0OO0O0O.html)

[USB_4大描述符](https://www.cnblogs.com/lifexy/p/7634511.html?tdsourcetag=s_pctim_aiomsg)

[USB组合设备 Interface Association Descriptor (IAD) ](https://www.cnblogs.com/shangdawei/p/4712305.html?tdsourcetag=s_pctim_aiomsg)

[USB 接口关联描述符](https://docs.microsoft.com/zh-cn/windows-hardware/drivers/usbcon/usb-interface-association-descriptor)

[WCID Device](https://github.com/xtoolbox/TeenyUSB/wiki/WCID-Device)

[STM32 USB设备远程唤醒机制详解](https://blog.csdn.net/weiaipan1314/article/details/113600998)

[USB协议学习](https://blog.csdn.net/auspark/article/details/107379939)

#USB Lib

[tinyusb](https://github.com/hathach/)

# HID Lib

[hidapi](https://github.com/signal11/hidapi)

[usbhid](https://github.com/uglycoder/usbhid) 

[USB学习：hidapi库使用](https://blog.csdn.net/subfate/article/details/104289106)

[HID设备数据格式](https://blog.csdn.net/chongchong0929/article/details/100939238)

[Qt USB通讯--hidapi的运用](https://wwwofai.com/article/2715)

# HID

[STM32 USB HID 组合设备（配置多个接口）](https://blog.csdn.net/dragon_cheng/article/details/83146547)

[STM32 USB复合设备编写](https://www.cnblogs.com/WeyneChen/p/6007049.html)

[USB 协议分析之 HID 设备](https://blog.csdn.net/zhoutaopower/article/details/82469665)

[8120年了，USB键盘还只能6键无冲？](https://www.bilibili.com/read/cv1480188/)

[USBHID协议解析](https://www.csdn.net/tags/MtTaMg0sMTM1OTctYmxvZwO0O0OO0O0O.html)

[USB HID Report 数据解析方法](http://blog.chinaunix.net/uid-598887-id-5593294.html)

[USB自定义HID设备实现-LPC1768](https://www.cnblogs.com/dengxiaojun/p/4357800.html)

#WinUSB

[WINUSB STM32移植参考](https://blog.csdn.net/wandersky0822/article/details/120729844?spm=1001.2101.3001.6650.3&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-3.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-3.pc_relevant_default&utm_relevant_index=6)

[求助：有没有STM32F0 USB VCP+HID复合设备例程](https://www.amobbs.com/thread-5722034-1-1.html)

#书籍

[我的新书《圈圈教你玩USB》的目录已经整理出来了](https://www.eefocus.com/computer00/blog/08-09/155814_4f1d0.html)

[圈圈教你玩USB光盘资料](https://github.com/GourdBrothers/USB-with-PIDUSBD12)

[圈圈教你玩usb 第2版 pdf](https://www.32r.com/soft/15715.html)

[圈圈教你玩USB.刘荣.插图版.PDF](http://www.ctfile.cn/archives/11261)

[圈圈教你玩USB光盘资料](https://github.com/GourdBrothers/USB-with-PIDUSBD12)

[《圈圈教你玩USB》 第一章 USB 概述及协议基础——看书笔记](https://blog.csdn.net/sz189981/article/details/62072621?utm_source=blogxgwz3)

[《圈圈教你玩USB》 第三章 USB鼠标的实现——看书笔记( 2 )](https://blog.csdn.net/sz189981/article/details/65029809)


