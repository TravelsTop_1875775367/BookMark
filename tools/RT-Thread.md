# RT-Thread

[I/O 设备模型](https://www.rt-thread.org/document/site/#/rt-thread-version/rt-thread-standard/programming-manual/device/device)

[RT-Thread Nano 简介](https://www.rt-thread.org/document/site/#/rt-thread-version/rt-thread-nano/an0038-nano-introduction)

[RT-Thread Studio使用记录（五）USB 虚拟串口](https://club.rt-thread.org/ask/article/efe4b393264b7d68.html)

[RT-Thread Studio使用记录（十一） 贪吃蛇+俄罗斯方块](https://club.rt-thread.org/ask/article/17c9c12174154589.html)
