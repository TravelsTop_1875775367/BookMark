
# Qt

[What's New in Qt 5.15](https://doc.qt.io/qt-5/whatsnew515.html)

[Qt Downloads](https://download.qt.io/official_releases/qt/5.12/5.12.12/)

[Qt 调用USB HID设备读写](https://blog.csdn.net/u012902367/article/details/84328341)

[Qt 键盘事件（捕获键盘按下、松开事件）](https://www.cnblogs.com/ybqjymy/p/14184163.html)

[QT 学习之键盘事件（ keyPressEvent）](https://blog.csdn.net/yexiangCSDN/article/details/80337491)

[Qt 获取键盘输入](https://www.cnblogs.com/herd/p/11062453.html)

[Qt中QPushButton、QTabWidget等部件内容文字换行的解决方法](https://blog.csdn.net/chaijunkun/article/details/6546704)

[Qt5.9设置pushButton按钮背景色和字体颜色](https://blog.csdn.net/naibozhuan3744/article/details/79397040)

[如何设置QPushButton背景透明样式如QLabel](https://jingyan.baidu.com/article/3d69c55172f5e7f0cf02d700.html)

[Qt基础之菜单栏](https://www.cnblogs.com/kyzc/p/11962903.html)

[Qt-QPainter实现动图与时钟](https://blog.csdn.net/oxiooixo/article/details/106427881)

[QPAINTER+QTIMER 模仿一下简单的电池充电](https://www.freesion.com/article/5082156934/)

[Qt 常见的坑之Qt程序遇到printf不输出问题](https://blog.csdn.net/Black_Silencer/article/details/115216664)

[QT程序打安装包或者统合成一个单独的exe文件](https://www.cnblogs.com/edgarli/p/16029042.html)

#绘图

[28.QT-QPainter介绍](https://www.cnblogs.com/lifexy/p/9203929.html)

[QT-【转】2D编程](https://www.bbsmax.com/A/mo5kqy9Wzw/)

[QT-【转】2D编程](https://www.likecs.com/show-307010178.html)


#使用

[在QT中创建文件](https://www.cnblogs.com/zhangdewang/p/6838434.html)

[教你使用windeployqt工具来进行Qt的打包发布](https://blog.csdn.net/sinat_36264666/article/details/73305712)

[QString字符串截取(Section，正则表达式)](https://blog.csdn.net/LebronBear/article/details/119111254)

[C程序命令行解析以及QT命令行解析之QCOMMANDLINEPARSER](https://www.freesion.com/article/1798673921/)

[Qt之QListView和QStandardItemModel用法 note](https://www.cnblogs.com/pandamohist/p/15177473.html)

[详解RGB-HSB互相转换的算法](https://www.jianshu.com/p/e555b8861c1c)

[Qt5.15下载和安装教程](https://blog.csdn.net/QTkaifa/article/details/110382673)



