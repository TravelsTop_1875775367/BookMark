#uGUI

[μGUI](http://embeddedlightning.com/ugui/)

[UGUI GitHub](https://github.com/achimdoebler/UGUI)

[【GUI】mini开源GUI－uGUI快速使用（可适用于OLED、LCD等显示器件）](https://blog.csdn.net/qq_39587650/article/details/119491697)

[stm32f429移植uGUI](http://bbs.eeworld.com.cn/thread-609286-1-1.html)

[分享一个比较容易移植的uGUI，只有2个文件](http://www.openedv.com/forum.php?mod=viewthread&tid=270773&page=1&extra=)


# LVGL

[【LVGL学习之旅 01】移植LVGL到STM32](https://blog.csdn.net/qq_40831286/article/details/107633216)

[lvgl最新版本在STM32上的移植使用](https://www.eefocus.com/embedded/484678)

[LittlevGL_Demo](https://gitee.com/mzy2364/LittlevGL_Demo)

[手把手教你学LittleVGL](http://www.openedv.com/docs/book-videos/zdyzshipin/4free/littleVGL.html)

[Simulator on PC](https://docs.lvgl.io/master/get-started/platforms/pc-simulator.html)

[lvgl](https://github.com/lvgl/lvgl)

[Welcome to the documentation of LVGL!](https://docs.lvgl.io/master/index.html)

[lv_demos](https://github.com/lvgl/lv_demos)

[LVGL学习笔记（1）——搭建环境、模拟仿真（Windows+Visual Studio）](https://www.jianshu.com/p/9f250749cd6e)

[LVGL | lvgl最新版本在STM32上的移植使用](https://blog.csdn.net/zhengnianli/article/details/113931569)

[【现货发售】STM32MP157移植LVGL](http://blog.itpub.net/70009145/viewspace-2841249/)

#GuiLite

[GuiLite](https://gitee.com/idea4good/GuiLite/tree/master#https://gitee.com/idea4good/GuiLiteSamples/tree/master/HelloWave)

[为什么GuiLite是最简单的GUI库](https://zhuanlan.zhihu.com/p/68378809)


#U8G2

[NodeMCU 之 U8G2 库使用详解](https://blog.csdn.net/weixin_42880082/article/details/113107251)




