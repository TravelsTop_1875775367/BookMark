
# RaspberryPi

[Raspberry Pi Documentation](https://www.raspberrypi.com/documentation/computers/os.html)

[开始使用树莓派](https://projects.raspberrypi.org/zh-CN/projects/raspberry-pi-getting-started/0)

[PWM - Pulse-width Modulation](https://pinout.xyz/pinout/pwm)

[WiringPi](https://github.com/WiringPi/WiringPi)


# 文档

[树莓派4的GPIO接口介绍](http://www.basemu.com/raspberry-pi-4-gpio-pinout.html)

[树莓派系列 入门WiringPi库的PWM接口(C和Python)](https://www.icxbk.com/article/detail/1778.html)

[yahboom](https://www.yahboom.com/study/raspberry4B#)

[树莓派4B串口通信配置](https://www.freesion.com/article/3312433910/)





