# GitHub

[Arduino-FastLED-Music-Visualizer](https://github.com/justcallmekoko/Arduino-FastLED-Music-Visualizer)

[modular-led-controller-workstation](https://github.com/segfault16/modular-led-controller-workstation/blob/master/arduino/ws2812_controller/ws2812_controller.ino)

[Rainbowduino_Snake_Game](https://github.com/Seeed-Studio/Rainbowduino_Snake_Game)

[arduino-fastled-rainbow](https://github.com/leon-anavi/arduino-fastled-rainbow)

[Rainbow-HAT](https://github.com/allocom/Rainbow-HAT)

# 3D显示

[动图说明光栅立体画原理，涨知识了](https://www.sohu.com/a/320423633_100005661)

[动图说明光栅立体画原理，涨知识了](https://www.163.com/dy/article/EHJ3R8PD0511HMM1.html)

[在天桥小摊上看到的那些立体画，是怎么做出来的](https://baijiahao.baidu.com/s?id=1656493894221717469&wfr=spider&for=pc)

[关于裸眼3D立体显示技术原理分析和应用](https://m.elecfans.com/article/1027206.html)

[裸眼3D立体显示技术详解 - 全文](https://www.elecfans.com/xianshi/20160127401403_a.html)

[裸眼3D柱镜光栅技术大揭秘](https://zhuanlan.zhihu.com/p/367987073)

[https://zhuanlan.zhihu.com/p/367987073](https://www.sohu.com/a/361230066_292909)

#项目

[效果炸裂的光立方](https://www.bilibili.com/video/BV1Sp411f7xN/?spm_id_from=333.788.recommend_more_video.6&vd_source=11835a751655c10e71986aed75b92043)

[项目分享| 16x16x16 4096个RGB LED的光立方是一种怎样的神奇效果](https://mp.weixin.qq.com/s/-vrA73eecow4UjdC09H1Mg)

[项目分享| 16x16x16 4096个RGB LED的光立方是一种怎样的神奇效果](https://zhuanlan.zhihu.com/p/504828145)

[Mega-Cube](https://github.com/MaltWhiskey/Mega-Cube)

[music_light_cube](https://github.com/redchenjs/music_light_cube)

[LedCube](https://github.com/Leopard-C/LedCube)

## LookingGlass

[LookingGlass](https://blog.lookingglassfactory.com/tag/case-studies/)

[2D → 3D Conversions](https://docs.lookingglassfactory.com/licenses-and-code-redemptions/2d-3d-conversions):LookingGlass 官方文档

[How the Looking Glass Works](https://docs.lookingglassfactory.com/keyconcepts/how-it-works):LookingGlass 官方文档

[Looking Glass](https://github.com/Looking-Glass/):GitHub

[光栅立体画制作，小时候的3D立体变换卡，lenticular imagery，Miggs Burroughs](https://www.bilibili.com/video/BV1da4y1H7rC/?spm_id_from=333.337.search-card.all.click&vd_source=11835a751655c10e71986aed75b92043)

[真正的全息影像技术](https://www.bilibili.com/video/BV11x411w7wU/?spm_id_from=333.337.top_right_bar_window_custom_collection.content.click&vd_source=11835a751655c10e71986aed75b92043)

[天道光栅立体画教程 神奇立体psdto3d制作过程 实拍立体视频教程](https://www.bilibili.com/video/BV1G7411n7YS/?spm_id_from=333.337.search-card.all.click&vd_source=11835a751655c10e71986aed75b92043)

[【裸眼全息显示器!!】投影二次元老婆!打破次元壁?!参观研发公司现场Vlog](https://www.bilibili.com/video/BV1vJ41147ns/?spm_id_from=333.788.recommend_more_video.-1&vd_source=11835a751655c10e71986aed75b92043)

[LookingGlass-Holograms Are Coming to the Internet](https://blog.lookingglassfactory.com/announcements/holograms-are-coming-to-the-internet/)

[【官方双语】全息相框加显示器 只要300刀- Looking Glass Portrait上手#linus谈科技](https://www.bilibili.com/video/BV1tL411n7Eg/?spm_id_from=333.337.search-card.all.click&vd_source=11835a751655c10e71986aed75b92043)

[开箱测评|全国首个8K 32寸Looking Glass裸眼全息显示器！](https://www.bilibili.com/video/BV1nK4y1V7WE/?vd_source=11835a751655c10e71986aed75b92043)
