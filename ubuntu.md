
#系统设置

[Ubuntu下修改系统的默认启动级别](https://blog.csdn.net/xiangwanpeng/article/details/54407619): sudo systemctl set-default multi-user.target

[Ubuntu修改系统的默认启动级别](https://www.linuxidc.com/Linux/2017-06/144808.htm):sudo systemctl set-default multi-user.target

[Ubuntu18.04 关闭和开启图形界面](https://www.cnblogs.com/schips/p/10577464.html~)

[如何将Ubuntu 18.04升级到Ubuntu 20.04](https://www.linuxidc.com/Linux/2020-03/162584.htm)

[怎么手动升级更新ubuntu系统到最新版](https://jingyan.baidu.com/article/359911f5ad774057fe030684.html)

[尝试进行apt升级时，如何解决Ubuntu 18.04中libc6-dev-armhf-cross的错误？](https://qastack.cn/ubuntu/1079797/how-do-i-fix-an-error-with-libc6-dev-armhf-cross-in-ubuntu-18-04-when-trying-to)

[如何查看当前Ubuntu系统的版本](https://blog.csdn.net/mybelief321/article/details/9076331)

# 工具

[ubuntu下的串口调试助手](https://www.csdn.net/tags/MtTagg3sMDA3OTctYmxvZwO0O0OO0O0O.html)

#数据处理

[ubuntu生成ssh公钥以及查看](https://www.jianshu.com/p/0c88bcaaaf13)

#ssh

[Linux远程登录（SSH）与远程拷贝，老司机开车！！](https://blog.csdn.net/zyqblog/article/details/79239007)


~
