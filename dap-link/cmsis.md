[CMSIS-DAP](https://arm-software.github.io/CMSIS_5/DAP/html/index.html)

[Configure USB Peripheral](https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__ConfigUSB__gr.html)

[Cortex-M微控制器软件接口标准CMSIS详细内容](https://mp.weixin.qq.com/s?__biz=MzI4MDI4MDE5Ng%3D%3D&chksm=ebbbb413dccc3d0540bdc63757490367f4a431b4ab548ad016704b5f4e70cdd19ba7fb79feb1&idx=1&mid=2247490040&scene=21&sn=bb1a493edaa0adc0407289a13980b7b2#wechat_redirect)

[嵌入式调试器原理和各类调试器集锦（JLINK、STLINK、CCDEBUG）](https://www.cnblogs.com/yueqian-scut/p/5853834.html)

[wch-link替代st-link（实际支持大部分arm核芯片）](https://blog.csdn.net/u011781073/article/details/120481845)

[DAPlink 生成MDK源码及编译过程记录](https://www.cnblogs.com/xiaoyu-lin/p/14085860.html)

[CMSIS-DAP和J-Link、ST-Link是什么关系？](https://blog.csdn.net/best_xiaolong/article/details/105039591)

[DAPlink 生成MDK源码及编译过程记录](https://www.cnblogs.com/xiaoyu-lin/p/14085860.html)

[vllink_lite](https://github.com/vllogic/vllink_lite)

[openocd_cmsis-dap_v2](https://github.com/vllogic/openocd_cmsis-dap_v2)

[Black Magic Probe](https://github.com/blackmagic-debug/blackmagic)

[DAPLink-Brochure](https://github.com/LGG001/DAPLink-Brochure)

# CMSIS-DAP HID

[开源CMSIS-DAP，资料合集](https://www.csdn.net/tags/NtjaIgzsMTEzNDgtYmxvZwO0O0OO0O0O.html)

# cmsis dap ubuntu

[ubuntu系统下使用openocd (野火的仿真器)下载stm32f103程序](https://www.xiaopingtou.net/article-20569.html)

# Cannot Load Flash Programming Algorithm

[用cmsis-dap debugger烧录器报错Cannot Load Flash Programming Algorithm](http://www.51hei.com/bbs/dpj-204951-1.html)

[Cannot Load Flash Programming Algorithm ！错误解决方案，亲自验证过的几套方案](https://blog.csdn.net/qq_35671135/article/details/87865259)

[提示出错 cannot load flash programming algorithm !](https://blog.csdn.net/sq2013317/article/details/72830946)


#视频
[DAP LINK下载仿真器/开源自制](https://www.bilibili.com/video/BV1K44y1V7xx?spm_id_from=333.337.search-card.all.click)



